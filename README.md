# GAMINGONLINUX



## Welcome


Webpage dedicated to give information 
about gaming on linux and some tested games. 



##  Classic games

### xdemineur

![](media/1649700989/1649701783-screenshot.png) 

## Chess eboard

eboard allows to play on FreeChess (opensource) using Menu > Connect to FICS.

Enter to have a guest (running about 1.hour)

example:
````
match guestXYZK  
accept
````

![](media/1649700989/1649701745-screenshot.png) 


Alternatively, it works as well with the webbrowser, allowing multiplayer gaming over Net:

![](media/1650284079/1650287544-screenshot.png)

![](media/1650284079/ebook-chess.png)



### ioquake3 


ioquake3 on OpenSUSE Leap 15.3 AMD64 (e.g. Ryzen,...)

![](media/1637395028/1637395028-screenshot.png)
![](media/1637395028/1637395434-screenshot.png)
![](media/1637395028/1637395437-screenshot.png)

Showing DM7 (DEMO Packs)

````
cd
mkdir .q3a ; cd .q3a 
wget -c --no-check-certificate   "http://files.retropie.org.uk/archives/Q3DemoPaks.zip"
unzip Q3DemoPaks.zip
````

Linux localhost.localdomain 5.3.18-57-default #1 SMP Wed Apr 28 10:54:41 UTC 2021 (ba3c2e9) x86_64 x86_64 x86_64 GNU/Linux

Machine Ryzen 3, AMD.
Game and Graphics working.  


### DOOM III (dhewm3)

For AMD64, Vulkan and Intel processor.

https://gitlab.com/openbsd98324/doom3


![](media/1642362556-screenshot.png)

![](media/1642363147-screenshot.png)



It might be recommended DHEWM3, but still: Doom3 native, rbdoom3bfg

rbdoom3bfg needs the website package, but it will work fine with dhewm3 (doom3)

dhewm3 is a great engine.


## Gaming on Notebook 

For  the graphic card on  
Intel PC, e.g. eeePC, R206S, 
the use of the mother board (intel) is possible. 


The games gzdoom on OpenSuse 15.3 Leap works. 
The graphics are working fine. 

See rootfs:
````
 wget -c --no-check-certificate   "https://gitlab.com/openbsd98324/opensuse-leap-15.3/-/raw/main/rootfs/opensuse-leap-15.3-amd64-x86_64-preinstalled-KDE-notebook.tar.gz" 
````


Games on Intel:
- Ennemy Territory
- gzdoom 
- supertuxkart 
(...) 


Example of Multiplay using gzdoom on opensuse. For multiplay on Lan, the very same version of gzdoom, lzdoom, zdoom is needed.
Only prboom-plus can allow to play better several version, on pi <-> opensuse amd64 (...).
````
 gzdoom -netmode 0 -port 5030 -host 2 -iwad doom2.wad -file Vanguard.wad
````

gzdoom works better on opensuse, and the version 3.2 of ascii devuan amd64 is old and bit laggy. 




###  Enemy Territory: Quake Wars (etqw) 


Native, contrib/non-free

Running on Debian 11.2 i386 with Intel (e.g. CPU N3050,...).

Kernel 5.11. with firmware (non-free) from the live cdrom.


Running well on Intel notebook, using devuan.
Ennemy Territory needs to be tested on Ryzen III. 




###  Return to Castle Wolfenstein (rtcw) 

FPS 





###  Eternal Lands 

Online multiplayer gaming 

Running on Raspberry pi 4 very well.

![](https://upload.wikimedia.org/wikipedia/en/b/bf/Eternal_Lands_fight.jpg)




###  Mana-Plus 

Online multiplayer gaming 

Running on PI3 and PI4.

Needs GL on RPI3.

![](https://manaplus.org/_media/manaplus_screenshot_8.png?cache=&w=900&h=600&tok=9c791d)




### Supertuxkart 

Running on PI4.


![](media/1671349338-1-supertuxkart-pi4.jpg)


Excellent graphics on AMD64:

![](media/SuperTuxKart-Scrot-1.jpg)
![](media/SuperTuxKart-Scrot-2.jpg)
![](media/SuperTuxKart-Scrot-3.jpg)
![](media/SuperTuxKart-Scrot-4.jpg)
![](media/SuperTuxKart-Scrot-5.jpg)
![](media/SuperTuxKart-Scrot-6.jpg)



### Supertux 

![](media/supertux.jpg)


Running on PI3 and PI4.






### Minetest


Linux localhost.localdomain 5.3.18-57-default #1 SMP Wed Apr 28 10:54:41 UTC 2021 (ba3c2e9) x86_64 x86_64 x86_64 GNU/Linux

Machine Ryzen 3, AMD.
Game and Graphics working.  

With registration, minetest is no longer fun !


![](media/1637401023/1637400961-screenshot.png)
![](media/1637401023/1637401005-screenshot.png)
 

Running on PI3 and PI4.


### PrBoom-Plus



Working well.


````
  prboom-plus -iwad freedoom2.wad 
````

Direct link: https://gitlab.com/openbsd98324/freedoom/-/raw/main/wad/freedoom2.wad

![](media/1637401255/1637401263-screenshot.png)



### zdoom/gzdoom 


OpenSUSE Leap has gzdoom, devuan older 3.x gzdoom on devuan ascii

````
  zypper install zdoom 


   gzdoom 
````

![](media/1637401255/1637401481-screenshot.png)





## Archlinux  

1.) Archlinux with Sauerbraten

 Sauerbraten with multiplayer

 Wolfeinstein enemy territory (see repos.) with ./etl 


 2.) Archlinux 

 Sauerbraten, PS 2 Emulator (pcsx2)

 (...)

Sauerbraten seems to not be present in leap opensuse by default.


 3.) devuan AMD64

 ioquake3 on intel

 gzdoom with 3.2. on devuan ascii 
 https://gitlab.com/openbsd98324/doom-mods

GZdoom mods:

![](https://gitlab.com/openbsd98324/doom-mods/-/raw/master/pub/gzdoom/3.2.4/GoldenSouls_Full_1.4.png)

(...)








## Retro Atari St (hatari)

hatari with tos102uk, running on Linux, BSD, all Raspberry PI models:  

PacMania:

![](media/1649700989/hatari-pacman.png)

 



## Gaming on Raspberry PI (2,3,...)

The Pi Zero W can emulate with retropie NES; SMS very well.
Note: PSX  <- eventually.

RPI3 and RPI4 offer greater gaming possibilities. 



- OpenMW, "Open Morrowind):

Natively running on RPI3 and RPI4.

![](media/1638706039-screenshot.png)


- Dosbox

Great collection of games for the PI, under DOSBOX. 

C&C 1995, running on framebuffer (fullsize) on RPI4 with retropie.


````
imgmount d: cdrom_gdi_1.iso -t iso 
cd cnc
c&c
````

![](media/1643612870-1-dosbox-cnc-1995-gdi-dos-v1.png)

(...)



- Quake III Arena (q3a):

RPI3 and RPI4.

![](media/q3a.jpg)



- fceux, ppsspp, ... and many more are available at retropie.
  
  ppsspp runs ok under RPI4. ppsspp can be slow on RPI3.

  gamecube runs ok under RPI4 for few games.




- netpanzer 

  It works very well on RPI3 and RPI4 (both PI models) under X11.

![](media/1649700989/netpanzer.png)







## Retropie (Tested for RPI3b and RPI3b+, Pi Zero w)

The image of Retropie (Tested and working !!) is at following address:

"https://github.com/RetroPie/RetroPie-Setup/releases/download/4.5.1/retropie-4.5.1-rpi2_rpi3.img.gz" 


![](media/1649700989/retropie-carbon-logo-1.png)


````
amiga
amstradcpc
arcade
atari2600
atari5200
atari7800
atari800
atarijaguar
atarilynx
dreamcast
fba
fds
gamegear
gb
gba
gbc
genesis
mame-libretro
mastersystem
megadrive
n64
neogeo
nes
ngp
ngpc
pcengine
psx
sega32x
segacd
sg-1000
snes
vectrex
zxspectrum
````


![](media/1671346073-1-gamingpan.jpg)


For RPI 3:

https://github.com/RetroPie/RetroPie-Setup/releases/download/4.8/retropie-buster-4.8-rpi2_3_zero2w.img.gz



For RPI 4:

https://github.com/RetroPie/RetroPie-Setup/releases/download/4.8/retropie-buster-4.8-rpi4_400.img.gz




**flemu on RPI3**
 
![](image/flemu-v0.2.png)

 Compile on Linux and NetBSD: 

  g++ -lm   -I"/usr/X11R7/include/" -I"/usr/pkg/include" -I /usr/pkg/include/    -L"/usr/pkg/lib"  -L/usr/X11R7/lib -lX11 -I /usr/X11R7/include/   -lfltk   source/fltk/flemu.cxx   -o   source/bin/flemu 



**GBA on RPI3**

![](media/mgba-lego-starwars-2.png)


**PSX on RPI3**


![](media/flemu-scars.png)

![](media/flemu-scars-2.png)


![](media/1631957988-screenshot.png)
![](media/1631958583-screenshot.png)
![](media/1631958638-screenshot.png)
![](media/1631958680-screenshot.png)
![](media/screenshot-1631957253.png)


- Bomberman World and Bomberman Party 

(/usr/games/pcsxr)

![](media/1652006524/1652006400-1-bomberman-psx.png)

![](media/1652006524/1652006449-1-bomberman-psx.png)



## Retropie on RPI4 (Raspberry PI 4)


Retropie for PI 4 will install RetroPie for Raspberry PI 4, RPI4 of all models. The choice of RetroPie is very good. 

Image retropie:
https://github.com/RetroPie/RetroPie-Setup/releases/download/4.8/retropie-buster-4.8-rpi4_400.img.gz
(note: apt-get needs a fix.)
 

The possibilities on **RPI4**, later on with it are: **0ad**, **Quake 3** (with GL1), **Supertuxkart**, **Cube-2** (one can add texture later), **Boswars**, Gamecube emulation (build dolphin-emu), **netpanzer**,... and so on. The graphics (GL) will work well on RetroPie. RPI4 is faster than RPI3, which allows more possibilities for gaming. 

- GTA Vice City

https://sites.google.com/view/raspberrypibuenosaires/gta-vice-city-en-raspberry-pi

Raspberry PI 4, 4GB


![](medias/1692356773-screenshot.png)


https://www.youtube.com/watch?v=dZCLkU0g0-A


- Eternal Lands works on rpi4 very well. 

![](https://upload.wikimedia.org/wikipedia/en/b/bf/Eternal_Lands_fight.jpg)

- Cube 2 / Sauerbraten works very fine on Raspberry pi 4, including multiplayer online gaming.

![](media/1652004243-1-venice-sauerbraten-cube2-pi.png)







**Games with lr-ppsspp on retropie:**


PSP and Dreamcast work very well on RPI 4 (Note: on RPI3 it could be slow.)

Tetris on lr-ppsspp works on rpi3b.



GTA on PSP, with files into ~/RetroPie/roms/psp/   <-- iso files.


GTA, Vice City (running well von RPI4):
![](media/1650284079/1650284565-screenshot.png)
![](media/1650284079/1650284025-screenshot.png)


GTA, Liberty City:
![](media/1650284079/1650283995-screenshot.png)


(Note: Liberty city on pi 4 could be slow, Vice city is ok.)


Tetris on PSP (PSN) (running on PI4 and too on PI3):
![](media/1650284079/1650285294-screenshot.png)


Pac-Man World (3d) (PSP) (running well von RPI4):
![](media/1650284079/1650284475-screenshot.png)


Daxter (PSP) (running well von RPI4): 
![](media/1650284079/1650292202-screenshot.png)

![](media/1650284079/1650292602-screenshot.png)







**Dreamcast (DC)**

On retropie, run : 
pi setup  and compile from source lr-flycast (expt).

lr-flycast allows to play dreamcast numerous game on RPI4:
virtua striker, Rayman 2,... 
(it requires little change in config for gamepad up/down/left/right). 


- Virtua Striker 2:
![](media/1650284079/1650289443-screenshot.png)

![](media/1650284079/1650289617-screenshot.png)


- South Park running well on RPI4 (running as well on PI3!)
![](media/1652004801-1-southpark-dreamcast.png)


- Test Drive - V Rally v1.x NTSC US (tested)   

- virtual tennis 2 (tested) 

- Toy Racer v1.050 (2000)(Sega)(PAL) (tested, but unf. it is laggy on rpi4)

- virtual striker2 (tested)  

- 4x4 jam (tested)  

- Roadsters v1.007 (2000)(Titus)(NTSC)(US) (tested and nice 3d car game). 
 e.g.
```` 
 /opt/retropie/emulators/retroarch/bin/retroarch -L /opt/retropie/libretrocores/lr-flycast/flycast_libretro.so --config /opt/retropie/configs/dreamcast/retroarch.cfg /home/pi/RetroPie/roms/dreamcast/dc2/Roadsters v1.007 (2000)(Titus)(NTSC)(US)[!].gdi --appendconfig /dev/shm/retroarch.cfg
```` 





For following, it needs a little change with the controller of lr-flycast (libretro):

Rayman 2 on DC: 
![](media/1650284079/1650290701-screenshot.png)

![](media/1650284079/1650291284-screenshot.png)

Reconfigure lr-flycast to have the D-pad working (on logitech F710):

![](https://gitlab.com/openbsd98324/dreamcast/-/raw/main/medias/IMG_7316.jpg)


- Donald going quacker

(DC: and so on ...)


## **Gamecube** on raspberry pi 4, rpi4

Further possibilities with recompiling from Source.
````
gamecube <--- only on rpi4  with recompilation / build from source
````


1. Get the file.

apt-get install xz-utils

https://downloads.raspberrypi.org/raspios_arm64/images/raspios_arm64-2022-09-26/2022-09-22-raspios-bullseye-arm64.img.xz

xzcat 2022-09-22-raspios-bullseye-arm64.img.xz  > /dev/...

2. Installation


````
apt-get update 
apt-get install --no-install-recommends ca-certificates qtbase5-dev qtbase5-private-dev git cmake make gcc g++ pkg-config libavcodec-dev libavformat-dev libavutil-dev libswscale-dev libxi-dev libxrandr-dev libudev-dev libevdev-dev libsfml-dev libminiupnpc-dev libmbedtls-dev libcurl4-openssl-dev libhidapi-dev libsystemd-dev libbluetooth-dev libasound2-dev libpulse-dev libpugixml-dev libbz2-dev libzstd-dev liblzo2-dev libpng-dev libusb-1.0-0-dev gettext


git clone https://github.com/dolphin-emu/dolphin.git

cd dolphin 

//next instruction is new
// git submodule update --init     
git submodule update --init --recursive


mkdir build && cd build
cmake ..
make
make install 
````

whereis dolphin-emu
dolphin-emu: /usr/local/bin/dolphin-emu



![](media/2022-12-18-033144_1920x1080_scrot.png)


![](media/2022-12-18-075343_1920x1080_scrot.png)




Mario Double Dash works fine on RPI4. 


![](media/2022-12-18-104247_1920x1080_scrot.png)
Linux raspberrypi 5.14.0-4-arm64




Ref. 

https://forums.raspberrypi.com/viewtopic.php?t=322606# 

https://raspi.debian.net/daily/raspi_4_bookworm.img.xz




## **DOSBOX:**

Screamer 2: 

![](media/1653206240-1-screamer-2.png)

sound: gold 
````
imgmount d: screamer2.cue -t iso 
````





**GTA:**

https://www.thomaslaurenson.com/blog/2021-03-05/gta3-on-the-retropie/


![](media/1652002625-1-gta-gameplay.png)

## Halo Combat Evolved

Game running on OpenSUSE Leap, on AMD64 Ryzen III, x86 64

````
zypper install wine 
````

384fb2afa7815109a7e06a3e43bcb092 HALO.iso

Mount the cdrom to /media/cdrom, with :

````
mkdir /media/cdrom ; mount HALO.iso /media/cdrom
````


Then, copy mfc42 dll and pidgen.dll

Copy the files mfc42 and pidgen.dll into ~/.wine/drive_c/windows/system32

6bc8b5736ba5537b3bbd0746ba508f46 pidgen.dll

8d0dbf25d91aa1be1e4e348434fd12e4 mfc42.dll

acf4dcccef25f9213a11bb88e9be2fac mfc42u.dll

Then, install Halo Evolved

````
 cd /media/cdrom ; wine Setup
````


Then, patch Halo Evolved with

````
 https://web.archive.org/web/20180605162448if_/http://halo.bungie.net/images/games/halopc/patch/110/halopc-patch-1.0.10.exe
````


Then, run the game: 
````
 cd ; cd .wine/drive_c/... 
````
And, type : wine halo.exe
 ￼


![](media/halo1-pc.png)



## Quake 1

Game running on OpenSUSE Leap, on AMD64 Ryzen III, x86 64

````
zypper install quakespasm
````


85fc9cee2035b66290da1e33be2ac86b  /home/user/quake1/pak0.pak

d76b3e5678f0b64ac74ce5e340e6a685  /home/user/quake1/pak1.pak

````
cd quake1
quakespasm 
````

![](media/quake1.png)


Sauerbraten running on RPI4, raspberry pi 4, 4GB
![](media/1671261698-screenshot.png)





## Gaming on PC, Ryzen III, AMD

````
CPU: AMD Ryzen 3 3200G with Radeon Vega Graphics     (3593.32-MHz 686-class CPU)
  Origin="AuthenticAMD"  Id=0x810f81  Family=0x17  Model=0x18  Stepping=1
````

OpenSUSE with rootfs:
 "https://gitlab.com/openbsd98324/opensuse-leap-15.3/-/raw/main/rootfs/opensuse-leap-15.3-amd64-x86_64-preinstalled-KDE-notebook.tar.gz" 

````
mkfs.ext3 -F /dev/sda4 
mount /dev/sda4 /mnt ; cd /mnt ; tar xvpfz opensuse-leap-15.3-amd64-x86_64-preinstalled-KDE-notebook.tar.gz
chroot /mnt
zypper install dolphin-emu 
````


PacMan World:
![](media/1649700989/1649678149-screenshot.png)


The GPU, Accelerations, ... graphics are working. 



Further games: 

 Daggerfall 

 ioquake3 
 
 dhewm3 (doom 3, natively)

 ... and so on.


## Gaming on Notebook N3050

OpenSUSE Leap 15.3, xfce, with kernel  5.10. (chim.)
Linux localhost.localdomain 5.10.0-7-amd64 #1 SMP Debian 5.10.40-1 (2021-05-28) x86_64 x86_64 x86_64 GNU/Linux
BOOT_IMAGE=/boot/vmlinuz-5.10.0-7-amd64 root=/dev/sda4 rw
(modified fstab)

![](media/opensuse-leap-Screenshot_2022-12-18_05-09-12.png)

![](media/Screenshot_2022-12-18_05-12-56.png)

(N3050 machine, notebook, intel runnning rather ok.)



